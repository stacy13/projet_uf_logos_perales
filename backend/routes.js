const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')

const User = require("./models/User")
const Run = require('./models/Run')

router.get("/users", async (req, res) => {
    const users = await User.find()
    res.send(users)
})
router.post("/users", async (req, res) => {
    const saltRounds = 10;
    const user = new User({
        userName: req.body.userName,
        userLastName: req.body.userLastName,
        userMail: req.body.userMail,
        userPass: bcrypt.hashSync(req.body.userPass, saltRounds),
        userSignUpDate: req.body.userSignUpDate
    })
    await user.save()
    res.send(user)
})
router.patch("/users/:id", async (req, res) => {
    try {
        const user = await User.findOne({ _id: req.params.id})

        if (req.body.userMail) {
            user.userMail = req.body.userMail
        }

        if (req.body.userPass) {
            user.userPass = req.body.userPass
        }   

        if (req.body.userName) {
            user.userName = req.body.userName
        }

        if (req.body.userLastName) {
            user.userLastName = req.body.userLastName
        }


        await user.save()
        res.send(user)

    } catch {
        res.status(404)
        res.send({ error : "User doesn't exist !"})
    }
})
router.delete("/users/:id", async (req, res) => {
    try{
        await User.deleteOne({ _id : req.params.id})
        res.status(204).send()
    } catch {
        res.status(404)
        res.send({ error : "User doesn't exist !"})
    }
})

router.get("/runs", async (req, res) => {
    const runs = await Run.find()
    res.send(runs)
})
router.post("/runs", async (req, res) => {
    const run = new Run({
        runDate : req.body.runDate,
        runTime : req.body.runTime,
        runLength : req.body.runLenght,
        userID : req.body.userID,
        geoPoints : req.body.geoPoints
    })
    await run.save()
    res.send(run)
})
router.delete("/runs/:id", async (req, res) => {
    try {
        await Run.deleteOne({ _id: req.params.id})
        res.status(204).send()
    } catch {
        res.status(404)
        res.send({error  : "run doesn't exist"})
    }
})



module.exports = router