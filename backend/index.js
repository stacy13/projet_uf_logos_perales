// use rabitDB
// db.createUser({user: "admin",pwd: "root",roles: [ "readWrite", "dbAdmin" ]});
// npm i express

const express = require('express');
const mongoose = require('mongoose')
const routes = require('./routes')
const bodyParser = require('body-parser')

const connectionString = "mongodb+srv://Oznerol:oznerologos@rabit-bgwdl.mongodb.net/test?retryWrites=true&w=majority"

mongoose
    .connect(connectionString, { useNewUrlParser:true, useUnifiedTopology: true})
    .then(() => {
        const app = express()
        app.use(bodyParser.json())
        app.use("/api", routes)

        app.listen(3000 , () => {
            console.log("Serveur lancé")
        })
    })






