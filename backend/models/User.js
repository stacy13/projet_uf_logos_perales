const mongoose = require('mongoose')

const schema = mongoose.Schema({
    userName : String,
    userLastName : String,
    userMail : String,
    userPass : String,
    userSignUpDate : String
})

module.exports = mongoose.model('User', schema)