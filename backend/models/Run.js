

const mongoose = require('mongoose')


const schema = mongoose.Schema({
    runDate : Number,
    runTime : Number,
    runLength : Number,
    userID : String,
    geoPoints : [{
        pointOrder : Number,
        latitude : Number,
        longitude : Number
    }]
})

module.exports = mongoose.model('Run', schema)