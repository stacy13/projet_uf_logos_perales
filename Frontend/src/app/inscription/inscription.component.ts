import { Component, OnInit, Injectable } from '@angular/core';
import { FormControl } from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";


@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  userName = new FormControl('')
  userLastName = new FormControl('')
  userMail = new FormControl('')
  userPass = new FormControl('')
  userValidPass = new FormControl('')
  d = new Date()
  response = ""

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    
  }

  createProfile(){
    if (this.userName.value != "" && this.userLastName.value != "" && this.userMail.value != "" && this.userPass.value != "" && this.userValidPass.value != "") {
      
      if (this.userPass.value == this.userValidPass.value) { 
        const currenDate =  this.d.getFullYear() + "/" + this.d.getMonth() + "/" + this.d.getDate()
        let user = {
          userName: this.userName.value,
          userLastName: this.userLastName.value,
          userMail: this.userMail.value,
          userPass: this.userPass.value, 
          userSignUpDate: currenDate,

        }
        this.http.post("http://localhost:3000/api/users", user).subscribe( data => {
          this.response = "Vous êtes inscrit !"
        }, error => {
          console.log("Error", error)
        })

        console.log(user)
      } else {
        console.log("no password match")
        this.response = "Les mots de passe sont différents"
      }
    } else{
      console.log("missing input(s)")
      this.response = "Vous devez remplir tous les champs"
    }
  }

}
