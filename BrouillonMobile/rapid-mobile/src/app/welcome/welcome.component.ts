import { Component, OnInit } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { Connexion } from "../shared/connexion.models";

@Component({
    selector: "Welcome",
    templateUrl: "./welcome.component.html",
    styleUrls: ["./welcome.component.css"],
})
export class WelcomeComponent implements OnInit {
    constructor(private page: Page) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        this.page.actionBarHidden = true;
    }
}
