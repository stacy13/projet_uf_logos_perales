import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { Connexion } from "../shared/connexion.models";
import { catchError } from "rxjs/operators";

@Injectable({
    providedIn: "root",
})
export class ConnexionService {
    url = "http://localhost:3000/api/users";

    constructor(private http: HttpClient) {}

    posts: Observable<any>;

    login(login: Connexion) {
        let url: string = this.url;

        this.posts = this.http
            .post(url, login, {})
            .pipe(catchError(this.handleError));

        return this.posts;
    }
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error("An error occurred:", error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                    `body was: ${error.error}`
            );
        }
        // return an observable with a user-facing error message
        return throwError("Something bad happened; please try again later.");
    }
}
