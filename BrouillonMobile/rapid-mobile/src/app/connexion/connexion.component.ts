import { Component, OnInit } from "@angular/core";
import { Connexion } from "../shared/connexion.models";
import { ConnexionService } from "./connexion.service";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular";
import { Page } from "tns-core-modules/ui/page/page";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
    selector: "Connexion",
    templateUrl: "./connexion.component.html",
    styleUrls: ["./connexion.component.css"],
})
export class ConnexionComponent implements OnInit {
    userConnexion: Connexion;

    isLoggingIn = true;
    public loginResult: JSON = JSON;

    constructor(
        private router: Router,
        private routerExtensions: RouterExtensions,
        private page: Page,
        private connexionService: ConnexionService
    ) {
        this.userConnexion = new Connexion();
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    submit() {
        if (this.isLoggingIn) {
            this.login();
        }
    }

    login() {
        this.connexionService.login(this.userConnexion).subscribe(
            (response) => (
                (this.loginResult = response),
                response["status"] == 404
                    ? alert("Adresse mail ou mot de passe incorrect !")
                    : localStorage.setItem(
                          "user_token",
                          JSON.stringify(response["access_token"]).split('"')[1]
                      ),
                this.redirect(response["status"])
            ),
            (error) => console.error("Error!", error)
        );
    }

    redirect(status) {
        if (status == 200) {
            alert("Vous êtes connecté !");
            location.reload();
            this.router.navigate(["/home"]);
        }
    }
}

// clearFields() {
//     this.userConnexion.email = "";
//     this.userConnexion.password = "";
// }
