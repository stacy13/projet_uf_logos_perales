import { User } from "./user.model";
import { ListeGeoloc } from "./listeGeoloc.models";

export class Course {
    courseId: string;
    courseDate: Date;
    courseDuree: string;
    courseLongueur: string;
    utilisateurID: User;
    courseListeGeoloc: ListeGeoloc;
}
