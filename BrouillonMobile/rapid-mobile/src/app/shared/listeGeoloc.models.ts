export class ListeGeoloc {
    pointId: string;
    lattitude: Date;
    longitude: string;
}
