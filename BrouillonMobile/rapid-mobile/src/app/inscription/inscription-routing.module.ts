import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { InscriptionComponent } from "./inscription.component";

const routes: Routes = [{ path: "", component: InscriptionComponent }];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule],
})
export class InscriptionRoutingModule {}
