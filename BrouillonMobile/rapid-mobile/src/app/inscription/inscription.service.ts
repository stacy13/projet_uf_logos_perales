import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "../shared/user.model";

@Injectable({
    providedIn: "root",
})
export class InscriptionService {
    constructor(private http: HttpClient) {}
    observable: Observable<any>;

    addUser(user: [Partial<User>]) {
        this.observable = this.http.post(
            "http://localhost:3000/api/users",
            user
        );
        return this.observable;
    }
}
