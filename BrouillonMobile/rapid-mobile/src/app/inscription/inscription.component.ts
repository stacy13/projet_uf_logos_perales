import { Component, OnInit } from "@angular/core";
// import { Location } from "@angular/common";
// import * as ApplicationSettings from "tns-core-modules/application-settings";
// import { Router } from "@angular/router";
// import { InscriptionService } from "./inscription.service";
// import { User } from "../shared/user.model";
import { FormControl } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
// import { catchError, retry } from "rxjs/operators";

@Component({
    selector: "Inscription",
    templateUrl: "./inscription.component.html",
    styleUrls: ["./inscription.component.css"],
})
export class InscriptionComponent implements OnInit {
    // userName = new FormControl("");
    // userLastName = new FormControl("");
    // userMail = new FormControl("");
    // userPass = new FormControl("");
    // userValidPass = new FormControl("");
    public input: any;
    d = new Date();
    response = "";

    constructor(private http: HttpClient) {
        this.input = {
            userName: "",
            userLastName: "",
            userMail: "",
            userPass: "",
            userValidPass: "",
        };
    }

    ngOnInit(): void {}

    createProfile() {
        if (
            this.input.userName.value != "" &&
            this.input.userLastName.value != "" &&
            this.input.userMail.value != "" &&
            this.input.userPass.value != "" &&
            this.input.userValidPass.value != ""
        ) {
            if (this.input.userPass.value == this.input.userValidPass.value) {
                const currenDate =
                    this.d.getFullYear() +
                    "/" +
                    this.d.getMonth() +
                    "/" +
                    this.d.getDate();
                let user = {
                    userName: this.input.userName.value,
                    userLastName: this.input.userLastName.value,
                    userMail: this.input.userMail.value,
                    userPass: this.input.userPass.value,
                    userSignUpDate: currenDate,
                };
                this.http
                    .post("http://localhost:3000/api/users", user)
                    .subscribe(
                        (data) => {
                            this.response = "Vous êtes inscrit !";
                        },
                        (error) => {
                            console.log("Error", error);
                        }
                    );

                console.log(user);
            } else {
                console.log("no password match");
                this.response = "Les mots de passe sont différents";
            }
        } else {
            console.log("missing input(s)");
            this.response = "Vous devez remplir tous les champs";
        }
    }
}
