import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { InscriptionRoutingModule } from "./inscription-routing.module";
import { InscriptionComponent } from "./inscription.component";

@NgModule({
    imports: [NativeScriptCommonModule, InscriptionRoutingModule],
    declarations: [InscriptionComponent],
    schemas: [NO_ERRORS_SCHEMA],
})
export class InscriptionModule {}
