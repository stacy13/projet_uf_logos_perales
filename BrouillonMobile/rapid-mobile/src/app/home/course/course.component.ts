import { Component, OnInit } from "@angular/core";
import { Timer } from "./timer";
import { State } from "./state";

@Component({
    selector: "Course",
    moduleId: module.id,
    templateUrl: "./course.component.html",
    styleUrls: ["./course.component.css"],
})
export class CourseComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}

    // public _btnPlay: string = "Démarrer";
    public _timer: Timer = new Timer();
    public _state: State = new State();

    play() {
        this._timer.start();
        this._state.setPlay();
        // this._btnPlay = "Continuer";
    }
    stop() {
        this._timer.stop();
        this._state.setStop();
    }
    backward() {
        this._timer.reset();
        this._state.setBackward();
        // this._btnPlay = "Démarrer";
    }
}
