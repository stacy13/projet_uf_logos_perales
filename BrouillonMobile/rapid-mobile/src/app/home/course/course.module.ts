import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { CourseRoutingModule } from "./course-routing.module";
import { CourseComponent } from "./course.component";
// import { Timer } from "./timer";
// import { State } from "./state";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        CourseRoutingModule,
        // Timer,
        // State,
    ],
    declarations: [CourseComponent],
    schemas: [NO_ERRORS_SCHEMA],
})
export class CourseModule {}
