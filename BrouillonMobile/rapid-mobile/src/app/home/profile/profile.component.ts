import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: "Profile",
    moduleId: module.id,
    templateUrl: "./profile.component.html",
    styleUrls: ["./profile.component.css"],
})
export class ProfileComponent implements OnInit {
    constructor(private router: Router) {}

    logout() {
        console.log("Tentative de déconnexion");

        localStorage.removeItem("user_token");
        alert("Vous êtes déconnecté !");
        location.reload();
        this.router.navigate(["/home"]);
    }
    ngOnInit(): void {}
}
