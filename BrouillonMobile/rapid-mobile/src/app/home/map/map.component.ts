import { Component, OnInit } from "@angular/core";
import { Mapbox, MapboxViewApi } from "nativescript-mapbox";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { registerElement } from "nativescript-angular";
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "tns-core-modules/ui/enums/enums";

registerElement("Mapbox", () => require("nativescript-mapbox").MapboxView);
@Component({
    selector: "Map",
    moduleId: module.id,
    templateUrl: "./map.component.html",
    styleUrls: ["./map.component.css"],
})
export class MapComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}

    onMapReady(args) {
        let map: MapboxViewApi = args.map;
    }

    // onDrawerButtonTap(): void {
    //     const sideDrawer = <RadSideDrawer>applicationCache.getRootView();
    //     sideDrawer.showDrawer();
    // }
}
