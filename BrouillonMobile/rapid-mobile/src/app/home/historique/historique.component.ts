import { ItemEventData } from "tns-core-modules/ui/list-view";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { Component, OnInit } from "@angular/core";

@Component({
    selector: "Historique",
    moduleId: module.id,
    templateUrl: "./historique.component.html",
    styleUrls: ["./historique.component.css"],
})
export class HistoriqueComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}
}
