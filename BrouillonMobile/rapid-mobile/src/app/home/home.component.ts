import {
    Component,
    OnInit,
    AfterViewInit,
    ViewChild,
    ElementRef,
} from "@angular/core";
import { RouterExtensions } from "nativescript-angular";
import { AnimationCurve } from "tns-core-modules/ui/enums";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit, AfterViewInit {
    @ViewChild("menuContainer", { static: false }) menuContainer: ElementRef;
    menuIsOpen: boolean = false;
    menuItems: { name: string; path: string; color: string }[] = [
        { name: "Map", path: "/map", color: "#9f153b" },
        { name: "Profile", path: "/profile", color: "#9f153b" },
        { name: "Historique", path: "/historique", color: "#9f153b" },
        { name: "Course", path: "/course", color: "#9f153b" },
        { name: "Close", path: "", color: "#9f153b" },
    ];

    currentPath: string;

    constructor(private routerExtensions: RouterExtensions) {}

    ngOnInit(): void {}

    ngAfterViewInit(): void {
        this.initializeMenu();
    }

    navigateToPath(path: string) {
        // close menu if the path is the same as current path or path is empty (close has empty path)
        if (!path || path === this.currentPath) {
            this.closeMenu();
        } else {
            this.routerExtensions.navigate([path]).then(() => {
                this.closeMenu();
                this.currentPath = path;
            });
        }
    }

    initializeMenu() {
        // set the origin point for the rotation
        this.menuContainer.nativeElement.originX = 0;
        this.menuContainer.nativeElement.originY = 0;

        this.menuContainer.nativeElement.rotate = -90;
        this.menuIsOpen = false;
    }

    toggleMenu() {
        if (this.menuIsOpen) {
            this.closeMenu();
        } else {
            this.openMenu();
        }
    }

    openMenu() {
        this.menuContainer.nativeElement
            .animate({
                rotate: 0,
                curve: AnimationCurve.cubicBezier(1, 0.02, 0.45, 0.93),
                duration: 200,
            })
            .then(() => {
                this.menuIsOpen = true;
            });
    }

    closeMenu() {
        this.menuContainer.nativeElement
            .animate({
                rotate: -90,
                curve: AnimationCurve.cubicBezier(1, 0.02, 0.45, 0.93),
                duration: 200,
            })
            .then(() => {
                this.menuIsOpen = false;
            });
    }
}
