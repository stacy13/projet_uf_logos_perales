import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { HomeComponent } from "./home.component";

const routes: Routes = [
    {
        path: "",
        component: HomeComponent,
        children: [
            { path: "", redirectTo: "map", pathMatch: "full" },
            {
                path: "map",
                loadChildren: "./home/map/map.module#MapModule",
            },
            {
                path: "profile",
                loadChildren: "./home/profile/profile.module",
            },
            {
                path: "historique",
                loadChildren:
                    "./home/historique/historique.module#HistoriqueModule",
            },
            {
                path: "course",
                loadChildren: "./home/course/course.module#CourseModule",
            },
        ],
    },
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule],
})
export class HomeRoutingModule {}
