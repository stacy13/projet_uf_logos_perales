import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ConnexionComponent } from "./connexion/connexion.component";
import { InscriptionComponent } from "./inscription/inscription.component";
import { HomeComponent } from "./home/home.component";

const routes: Routes = [
    { path: "", redirectTo: "/welcome", pathMatch: "full" },
    // {
    //     path: "",
    //     redirectTo:
    //         "/(connexion:connexion//inscription:inscription//home:home)",
    //     pathMatch: "full",
    // },

    {
        path: "connexion",
        component: ConnexionComponent,
        // outlet: "connexion",
    },
    {
        path: "inscription",
        component: InscriptionComponent,
        // outlet: "inscription",
    },
    {
        path: "home",
        component: HomeComponent,
        // outlet: "home",
    },
    {
        path: "map",
        loadChildren: "./home/map/map.module#MapModule",
    },
    {
        path: "profile",
        loadChildren: "./home/profile/profile.module#ProfileModule",
    },
    {
        path: "historique",
        loadChildren: "./home/historique/historique.module#HistoriqueModule",
    },
    {
        path: "course",
        loadChildren: "./home/course/course.module#CourseModule",
    },
    {
        path: "welcome",
        loadChildren: () =>
            import("~/app/welcome/welcome.module").then((m) => m.WelcomeModule),
    },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule],
})
export class AppRoutingModule {}
