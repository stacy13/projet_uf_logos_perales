import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  navLinks: any[];

  constructor() {
    this.navLinks = [
      {
        label: 'Tableau de bord',
        link: '',
        index: 0,
      },
      {
        label: 'Compte',
        link: '',
        index: 1,
      },

      {
        label: 'Connexion - Inscription',
        link: '',
        index: 2,
      },
    ];
  }

  ngOnInit(): void {}
}
